# -*- coding: utf-8 -*-
# copyright 2024 LOGILAB S.A. (Paris, FRANCE), all rights reserved.
# contact https://www.logilab.fr -- mailto:contact@logilab.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 2.1 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""cubicweb-enervin specific hooks and operations"""

from cubicweb.server.hook import Hook
from cubicweb.predicates import is_instance
from cubicweb_enervin.notify_bottle_search import notify_bottle_search

class NotificationBottleSearch(Hook):
    __regid__ = "enervin.notify-bottle-search"
    __select__ = Hook.__select__ & is_instance("Bottle")
    events = (
        "after_add_entity",
        "after_update_entity",
    )

    def __call__(self):
        notify_bottle_search(self._cw, self.entity)
