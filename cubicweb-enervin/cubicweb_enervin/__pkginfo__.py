"""cubicweb-enervin application packaging information"""

modname = "cubicweb_enervin"
distname = "cubicweb-enervin"

numversion = (0, 1, 0)
version = ".".join(str(num) for num in numversion)

license = "LGPL"
author = "LOGILAB S.A. (Paris, FRANCE)"
author_email = "contact@logilab.fr"
description = "Enervin proto"
web = "https://forge.extranet.logilab.fr/cubicweb/cubes/enervin"

__depends__ = {
    "cubicweb": ">= 4.8.0, < 5.0.0",
    "cubicweb-web": None,
}
__recommends__ = {}

classifiers = [
    "Environment :: Web Environment",
    "Framework :: CubicWeb",
    "Programming Language :: Python :: 3",
    "Programming Language :: JavaScript",
]
