import logging

logger = logging.getLogger("Enervin-notify")
logger.setLevel(logging.DEBUG)

def notify_bottle_search(cnx, bottle):
    searches = cnx.execute("Any S WHERE S is BottleSearch").entities()
    for search in searches:
        resp = cnx.execute(
            "Any B WHERE B eid %(eid)s, " + search.rql_query,
            {"eid": bottle.eid},
        )
        if resp:
            user = search.user[0]
            logger.debug(f"NOTIFY {user.login} for bottle #{bottle.eid}")
            if user.primary_email:
                logger.debug(f"Send email to {user.primary_email[0].address}")
            else:
                logger.debug(f"There is no primary_email for user {user.login}")
