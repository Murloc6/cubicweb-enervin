# -*- coding: utf-8 -*-
# copyright 2024 LOGILAB S.A. (Paris, FRANCE), all rights reserved.
# contact https://www.logilab.fr -- mailto:contact@logilab.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 2.1 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""cubicweb-enervin schema"""


from cubicweb.schema import RQLConstraint
from yams.constraints import (
    IntervalBoundConstraint,
    RegexpConstraint,
)
from yams.buildobjs import (
    Date,
    EntityType,
    String,
    SubjectRelation,
    Int,
    Float,
)

from cubicweb_enervin.utils import EMAIL_ADDRESS_REGEXP

_positive = IntervalBoundConstraint(minvalue=0.0)


class WineType(EntityType):
    name = String(required=True)
    grape_varieties = SubjectRelation("GrapeVariety", cardinality="**")


class GrapeVariety(EntityType):
    name = String(required=True)
    hints = SubjectRelation("Hint", cardinality="**")


class Hint(EntityType):
    name = String(required=True)


class Vintage(EntityType):
    name = String(required=True)
    wine_type = SubjectRelation("WineType", cardinality="1*", inlined=True)
    year = Int()
    hints = SubjectRelation("Hint", cardinality="**")


class Bottle(EntityType):
    volume = Int(required=True, constraints=[_positive])  # into milimeter
    vintage = SubjectRelation("Vintage", cardinality="1*", inlined=True)


class WineProposition(EntityType):
    seller = SubjectRelation("Seller", cardinality="1*", inlined=True)
    bottle = SubjectRelation("Bottle", cardinality="1*", inlined=True)
    prices = SubjectRelation("Price", cardinality="*1")


class Price(EntityType):
    price = Float(required=True, constraints=[_positive])  # €
    date = Date(required=True)


class Seller(EntityType):
    name = String(required=True)
    address = String()
    sales = SubjectRelation("Sale", cardinality="*1")


class Sale(EntityType):
    date = Date(required=True)
    quantity = Int(required=True, constraints=[_positive])
    description = String()
    bottle = SubjectRelation("Bottle", cardinality="1*", inlined=True)
    price = Float(required=True, constraints=[_positive])  # €


class Expert(EntityType):
    name = String(required=True)
    company = String()
    email_address = String(constraints=[RegexpConstraint(EMAIL_ADDRESS_REGEXP)])
    reviews = SubjectRelation("Review", cardinality="*1")


class Review(EntityType):
    vintage = SubjectRelation("Vintage", cardinality="1*", inlined=True)
    mark = Int(
        required=True,
        constraints=[IntervalBoundConstraint(minvalue=0, maxvalue=5)],
    )
    comment = String()

class BottleSearch(EntityType):
    rql_query = String(required=True)
    user = SubjectRelation("CWUser", cardinality="1*", inlined=True)
