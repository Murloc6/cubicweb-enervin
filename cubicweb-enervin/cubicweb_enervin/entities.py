# -*- coding: utf-8 -*-
# copyright 2024 LOGILAB S.A. (Paris, FRANCE), all rights reserved.
# contact https://www.logilab.fr -- mailto:contact@logilab.fr
#
# This program is free software: you can redistribute it and/or modify it under
# the terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 2.1 of the License, or (at your option)
# any later version.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

"""cubicweb-enervin entity's classes"""
from typing import Dict, Literal

from datetime import date, timedelta

from cubicweb.entities import AnyEntity


class Vintage(AnyEntity):
    __regid__ = "Vintage"

    @property
    def average_mark(self):
        reviews = list(
            self._cw.execute(
                "Any R WHERE R is Review, R vintage %(eid)s",
                {"eid": self.eid},
            ).entities()
        )
        return sum(review.mark for review in reviews) / len(reviews)


class Bottle(AnyEntity):
    __regid__ = "Bottle"

    @property
    def average_mark(self):
        return self.vintage[0].average_mark

    def average_price_for_day(
        self,
        day: date,
    ) -> float:
        rset = list(
            self._cw.execute(
                "Any P WHERE WPROP prices PS, PS date MAXDATE, PS price P"
                " WITH MAXDATE, WPROP BEING ( "
                    "Any MAX(PSD), WP GROUPBY WP WHERE B eid %(eid)s, "
                    "WP is WineProposition, WP bottle B, WP prices PS, "
                    "PS date PSD, PS price P HAVING PSD <= %(date)s"
                ")",
                {"eid": self.eid, "date": day},
            )
        )
        sum_prices = 0
        if not rset:
            return 0
        for row in rset:
            sum_prices += row[0]
        return sum_prices / len(rset)

    def average_price_for_period_by_day(
        self,
        from_date: date,
        to_date: date,
    ) -> Dict[date, float]:
        delta = timedelta(days=1)
        price_stats = {}
        current_date = from_date
        while current_date < to_date:
            price_stats[current_date] = self.average_price_for_day(current_date)
            current_date = current_date + delta
        return price_stats
