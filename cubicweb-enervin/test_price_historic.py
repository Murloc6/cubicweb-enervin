from datetime import date

bottle = cnx.find("Bottle", volume=1500).one()
from_date = date(year=2024, month=5, day=21)
to_date = date(year=2024, month=5, day=30)
stats = bottle.average_price_for_period_by_day(from_date, to_date)
print(stats)
