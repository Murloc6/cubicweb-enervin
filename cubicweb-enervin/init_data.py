hint_fruitsrouges = cnx.create_entity("Hint", name="Fruits rouges")
hint_pierreasilex = cnx.create_entity("Hint", name="Pierre à silex")
hint_banane = cnx.create_entity("Hint", name="Banane")

grapevariety_negrette = cnx.create_entity("GrapeVariety", name="Négrette")
grapevariety_pinotnoir = cnx.create_entity("GrapeVariety", name="Pinot noir")
grapevariety_syrah = cnx.create_entity("GrapeVariety", name="Syrah")

winetype_fronton = cnx.create_entity("WineType", name="Fronton")
winetype_sancerre = cnx.create_entity("WineType", name="Sancerre")
winetype_chateau = cnx.create_entity("WineType", name="Châteauneuf-du-Pape")

vintage_chateau_2019 = cnx.create_entity(
    "Vintage",
    name="Châteauneuf-du-Pape 2019",
    wine_type=winetype_chateau,
    year=2019,
    hints=[hint_pierreasilex, hint_banane],
)
vintage_chateau_2020 = cnx.create_entity(
    "Vintage",
    name="Châteauneuf-du-Pape 2020",
    wine_type=winetype_chateau,
    year=2020,
    hints=[hint_fruitsrouges],
)
vintage_fronton_2021 = cnx.create_entity(
    "Vintage",
    name="Fronton 2020",
    wine_type=winetype_fronton,
    year=2021,
    hints=[hint_fruitsrouges],
)

bottle_chateau_800 = cnx.create_entity(
    "Bottle",
    volume=800,
    vintage=vintage_chateau_2020,
)
bottle_fronton_1500 = cnx.create_entity(
    "Bottle",
    volume=1500,
    vintage=vintage_fronton_2021,
)

seller_lidl = cnx.create_entity(
    "Seller",
    name="Lidl",
    address="42 rue d'ici, 00042",
)
seller_intermarche = cnx.create_entity(
    "Seller",
    name="Intermarché",
)

wineprop_1500_inter = cnx.create_entity(
    "WineProposition",
    seller=seller_intermarche,
    bottle=bottle_fronton_1500,
)
wineprop_1500_lidl = cnx.create_entity(
    "WineProposition",
    seller=seller_lidl,
    bottle=bottle_fronton_1500,
)

price_1500_lidl_42 = cnx.create_entity(
    "Price",
    price=42.0,
    date="2024-05-22",
)
price_1500_lidl_45 = cnx.create_entity(
    "Price",
    price=45.0,
    date="2024-05-24",
)
price_1500_lidl_50 = cnx.create_entity(
    "Price",
    price=50.0,
    date="2024-05-27",
)
wineprop_1500_lidl.cw_set(prices=[price_1500_lidl_42, price_1500_lidl_45, price_1500_lidl_50])

price_1500_inter_60 = cnx.create_entity(
    "Price",
    price=60.0,
    date="2024-05-27",
)
wineprop_1500_inter.cw_set(prices=[price_1500_inter_60])

sale_2_1500_lidl = cnx.create_entity(
    "Sale",
    date="2024-05-23",
    quantity=2,
    description="Commande pour un anniversaire",
    bottle=bottle_fronton_1500,
    price=84.0,
)
sale_1_1500_lidl = cnx.create_entity(
    "Sale",
    date="2024-05-23",
    quantity=1,
    bottle=bottle_fronton_1500,
    price=42.0,
)
seller_lidl.cw_set(sales=[sale_1_1500_lidl, sale_2_1500_lidl])

expert_jean = cnx.create_entity(
    "Expert",
    name="Jean Dupond",
    company="Indépendant",
    email_address="jean.dupond@ouioui.fr",
)
expert_jeanne = cnx.create_entity(
    "Expert",
    name="Jeanne Durand",
    company="Lidl",
    email_address="jeanne.durand@ouinon.fr",
)

review_jeanne_chateau_20_2 = cnx.create_entity(
    "Review",
    vintage=vintage_chateau_2020,
    mark=2,
    comment="bof, pas dingue"
)
review_jeanne_chateau_19_5 = cnx.create_entity(
    "Review",
    vintage=vintage_chateau_2019,
    mark=5,
    comment="le best",
)
expert_jeanne.cw_set(reviews=[review_jeanne_chateau_19_5, review_jeanne_chateau_20_2])

review_jean_chateau_20_3 = cnx.create_entity(
    "Review",
    vintage=vintage_chateau_2020,
    mark=3,
    comment="c'est ok",
)
review_jean_fronton_4 = cnx.create_entity(
    "Review",
    vintage=vintage_fronton_2021,
    mark=4,
    comment="il est ouf!",
)
expert_jean.cw_set(reviews=[review_jean_fronton_4, review_jean_chateau_20_3])

cwuser_admin = cnx.find("CWUser", login="admin").one()
admin_email = cnx.create_entity(
    "EmailAddress",
    address="admin.admin@coucou.Fr",
)
cwuser_admin.cw_set(primary_email=admin_email)

cnx.create_entity(
    "BottleSearch",
    rql_query='B vintage V, V hints H, H name "Fruits rouges"',
    user=cwuser_admin,
)
