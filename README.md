# Enervin application

Cette application est un prototype pour un backend de gestion de vin.
L'application est créée en utilisant le cadriciel [CubicWeb](https://www.cubicweb.org/).
Pour cela, un cube a été créé dans le dossier `cubicweb_enervin`.

## Installation

Tout d'abord, il est nécessaire de se positionner dans le dossier du cube.

```bash
cd cubicweb-enervin
```

Créer un environement virtuel Python pour installer les dépendances.

```bash
python -m venv .venv
source .venv/bin/activate
```

Ensuite, installer les dépendances du projet avec la commande

```bash
pip install -e .
```

Une fois les dépendances isntallées, il est maintenant possible de créer l'instance
de notre application avec la commande suivante :

```bash
cubicweb-ctl create enervin enervin-app
```

Nous créons ici une instance du cube qui se nomme `enervin` et nous nommons
l'instance `enervin-app`. Il est conseillé d'utiliser une base de données
de type `sqlite` pour des simplicité de déploiement.
Répondez aux questions suivantes suivant vos désirs. Si vous ne savez pas
quoi répondre, laissez la valeur par défaut.

L'application est installée, on peut maintenant la lancer

```bash
cubicweb-ctl start enervin-app
```

L'application est maintenant disponible à l'adresse `http://localhost:8080` (sauf
si vous avez changé le port).

## Données de test

Pour pouvoir manipuler des données, un fichier pemet d'initialiser la base avec
des données de test.
Ce fichier se trouve à la racine du cube et se nomme `init_data.py`.
Pour pouvoir le charger dans la base il faut lancer la commande suivante :
```bash
cubicweb-ctl shell enervin-app init_data.py
```

Vous pourrez observer, en rechargant la page de votre navigateur, que des données
sont chargées.

## Modèle de données

Le modèle de données peut être observer à l'URL `http://localhost:8080/schema` dont voici
le schéma :

![schéma modèle  de données](images/modele_donnees.png)

## Tests des fonctionnalités attendues

Des scripts de tests sont présents à la racine du cube pour s'assurer des fonctionnalités attendues.
(TODO : ajouter ces tests en tests unitaires)


### Test de la moyenne des notes des experts pour un cépage

Pour tester la moyenne des notes des experts pour un cépage (ici on regarde "Châteauneuf-du-Pape 2020")
```bash
cubicweb-ctl shell enervin-app test_average_review_mark.py
```
Cette commande devrait afficher "2.5" si aucune données n'a été changée depuis l'initialisation de celles ci.

### Test des notification

Pour tester la notification, nous modifions le volume d'une bouteil qui correspond à un critère mis dans notre
`BottleSearch`. Ici nous souhaitons que l'utilisateur "admin" soit notifié lorsqu'une bouteil est ajoutée ou
modifiée si cette bouteil correspond à un cépage qui a des notes de "Fruits rouges".
```bash
cubicweb-ctl shell enervin-app test_notification.py
```
Le résultat doit être
```
2024-05-30 00:21:49 - (Enervin-notify) DEBUG: NOTIFY admin for bottle #1016
2024-05-30 00:21:49 - (Enervin-notify) DEBUG: Send email to admin.admin@coucou.Fr
2024-05-30 00:21:49 - (Enervin-notify) DEBUG: NOTIFY admin for bottle #1016
2024-05-30 00:21:49 - (Enervin-notify) DEBUG: Send email to admin.admin@coucou.Fr
```
qui indique que la modification des paramètres de la bouteil a permis de notifier qu'il faut
envoyer un email à admin.admin@coucou.fr car cette personne est interessée par ce type de bouteil
(cépage avec des notes de "Fruits rouges").
Nous affichons 2 fois la notification car le script remet le volume à 800 comme initialement dans les données.

### Test de l'historique des prix pour une bouteil

Ce script permet d'afficher un dictionnaire qui contient, pour chaque jour dans la période sélectionnée,
la moyenne des prix pour toutes les propositions ce jour là.
Pour l'éxécuter il faut lancer la commande :
```bash
cubicweb-ctl shell enervin-app test_price_historic.py
```
Vous devriez observer :
```python
{datetime.date(2024, 5, 21): 0, datetime.date(2024, 5, 22): 42.0, datetime.date(2024, 5, 23): 42.0, datetime.date(2024, 5, 24): 45.0, datetime.date(2024, 5, 25): 45.0, datetime.date(2024, 5, 26): 45.0, datetime.date(2024, 5, 27): 55.0, datetime.date(2024, 5, 28): 55.0, datetime.date(2024, 5, 29): 55.0}
```
On observe que pour le 21 mai le prix est de 0 puisqu'il n'y a aucune
proposition à ce moment là. La première proposition est de 42€ à partir du 22
mai et ce jusqu'au 24, où le prix passe à 45€. A partir du 27 mai, le prix est
de 55 car il y a 2 propositions de prix dans les deux revendeurs, une à 50€ et
l'autre à 60€, la moyenne est donc de 55€.
